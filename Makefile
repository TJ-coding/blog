.PHONEY: docs

docs:
	cd Documentation; TZ=UTC9 mkdocs serve

install-docs:
	pip install -r Documentation/scripts/requirements.txt
	pip install  -r Documentation/scripts/widget_requirements.txt
	