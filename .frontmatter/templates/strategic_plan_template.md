---
# Meta Data
categories:
    - Strategic Plan
tags:
    - Strategic Plan
date: 2024-01-31
title: "Strategic Plan"
intent: "intent"
bottom_line_up_front: ["Point1", "Point2"]
# Directions
vision: ""
aim: ""
strategic_concept: ""
values: ""
prioritised_strategic_goals: [{'id': 'A', 'name': 'apple',  'success_indicators': 'hi', 'time_bound': 'T1', 'doing': False, 'done':False}]
specific_indicator_goals: [{'id': 'B', 'psg_id': 'A', 'name': 'banana', 'specific_measurable_criteria': 'bye', 'time_bound': 'T2', 'doing': False, 'done':False}]
event_table: [{'symbol':'T1', 'date':12-12-12'}]
decision_points: [{goal_id: 'A', 'success':[B], 'fail':['B'], 'success_description':'', 'fail_description':''}]

check_frequency: "1 Week"
check_schedules: []
---

# {{title}} 

<!--Type in what you have done-->
Intent: __{{intent}}__

<!--bottom line upfront-->
!!! strategic-plan "Bottom Line Up Front"
    {% for point in bottom_line_up_front %}
    * {{point}}
    {% endfor %}
<!-- more -->
---

## Strategic Goal Planner
<!--Directions-->
<table markdown>
<tr markdown><td markdown="block" colspan=2>
## :material-sign-direction: Direction</td></tr>
<tr markdown>
<td markdown> __Vision__ </td> 
<td markdown="block">{{vision}}
</td>
</tr>
<tr markdown>
<td markdown>__Aim__</td> 
<td markdown="block">{{aim}}  </td>
</tr>
<tr markdown>
<td markdown>__Strategic Concept__</td> 
<td markdown="block">{{strategic_concept}}  </td>
</tr>
<tr markdown>
<td markdown>__Values__</td> 
<td markdown="block">{{values}}</td>
</tr>
</table>
---


## :octicons-goal-24: Cascading Goal Diagram
{{cascading_goal_diagram(title, prioritised_strategic_goals, specific_indicator_goals)}}

{{prioritised_strategic_goal_table(prioritised_strategic_goals,
event_table)}}

{{specific_indicator_goal_table(prioritised_strategic_goals, specific_indicator_goals, event_table)}}

---


<!--Decision Point Matrix-->
{{decision_point_matrix_table(decision_points)}}

<!-- DECISION TREE DIAGRAM -->
## :material-arrow-decision: Decision Tree Diagram
{{decision_tree_diagram(prioritised_strategic_goals, specific_indicator_goals, event_table, decision_points)}}

---
{{event_time_table(event_table)}}
---
{{check_schedule_log(check_frequency, check_schedules)}}

<!--Makes the sidebar on the left smaller-->
<style>
    @media only screen and (min-width: 76.25em) {
  .md-main__inner {
    max-width: none;
  }
  .md-sidebar--primary {
    left: 0;
  }
  .md-sidebar--secondary {
    right: 0;
    margin-left: 0;
    -webkit-transform: none;
    transform: none;   
  }
}
</style>

<!--Forces Table Width to be 100%-->
<style>
.md-typeset__table {
   min-width: 100%;
}

.md-typeset table:not([class]) {
    display: table;
    font-size: 0.75rem;
}
</style>