---
# Meta Data
categories:
    - Strategic Plan
tags:
    - Strategic Plan
date: 2024-01-31

# Headings
title: Lecture Summarization
intent: Summarize lecture videos such that, we can identify preliminary knowledge just in time and read ahead of lectures.
---

# {{title}} 

<!--Type in what you have done-->
Intent: __{{intent}}__
