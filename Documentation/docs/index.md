# Blog
<div class="grid" markdown>


!!! labbook "Labbook"
    Logs of the experiments can be found in Labbook.

    [Go to Labbook](blog/category/labbook/){ .md-button }


!!! strategic-plan "Strategic Plan"
    Plans of this project can be found in Strategic Plan.
    
    [Go to Strategic Plans](blog/category/strategic-plan/){ .md-button }

!!! success "Kanban Board"
    Glorified todo list. Backlog of tasks that results in increments of a working software is kept and prioritized.
    
    [Go to Kanban Board](kanban){ .md-button }

</div>

??? Example "Installation"
    ## Install
    ```bash 
    pip install -e ./blog
    ```
    If you want to build the documentation on local machine: 
    ```bash
    pip install -r Document/scripts/requirements.txt
    ```
    ### Uninstall
    ```bash
    pip uninstall blog
    ```

## Commands
### Makefile

* `make docs` - serves and opens the documentation. Open `http://127.0.0.1:8000` with a browser.

* `make gitlab` - creates a new private repository on gitlab.

### MkDocs
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

``` shell
📁 naist-lecture-transcript
    ├── 📁 naist-lecture-transcript
        ├── 📁 Data
            ├── 📁 Raw          # (1)!
            ├── 📁 Processed    # (2)!
            └── 📁 Dataset      # (3)!
```

1.  Contains Immutable Downloaded Data
2.  Contains Processed Data Interpretable by Human
3.  Contains Dataset for Model Consumption

## Recommended VS Code Setup

1. Open Simple Browser

    1. Ctr + Shift + P: "simple browser"
    2. Open http://127.0.0.1:8000/
    3. Pin the tab

2. Open Task Board

    1. Install the VS COde Extension: `coddx.coddx-alpha`
    2. Ctr + Shift + P: "Coddx: TODO.md Kanban Task Board"

[:material-view-list: Bag of Materials](bom.md)